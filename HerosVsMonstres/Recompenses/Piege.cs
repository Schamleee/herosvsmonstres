﻿using HerosVsMonstres.Personnages;
using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Recompenses
{
    public class Piege : IRecompense
    {
        public void recompenser(Personnage v_personnage)
        {
            Random nbAleatoire = new Random();

            int nbPvPerdus = nbAleatoire.Next(1, 10);
            v_personnage.infligerMalus(nbPvPerdus);
            Console.WriteLine("J'ai fait les frais d'un piège de {0} PV !", nbPvPerdus);
        }
    }
}
