﻿using HerosVsMonstres.Personnages;
using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Recompenses
{
    public class Tresor : IRecompense
    {
        public void recompenser(Personnage v_personnage)
        {
            Random nbAleatoire = new Random();

            int nbPvAcquis = nbAleatoire.Next(10, 20);
            v_personnage.appliquerBonus(nbPvAcquis);
            Console.WriteLine("J'ai bénéficié d'un bonus de {0} PV !", nbPvAcquis);
        }
    }
}
