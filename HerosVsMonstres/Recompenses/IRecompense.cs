﻿using HerosVsMonstres.Personnages;
using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Recompenses
{
    public interface IRecompense
    {
        public void recompenser(Personnage personnage);
    }
}
