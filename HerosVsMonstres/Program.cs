﻿using HerosVsMonstres.Personnages.Heros;
using System;

namespace HerosVsMonstres
{
    class Program
    {
        static void Main(string[] args)
        {
            Heros monHeros = new Heros(100, 50);

            MoteurDeJeu moteurDeJeu = new MoteurDeJeu(monHeros);
        }
    }
}
