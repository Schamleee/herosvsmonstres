﻿using HerosVsMonstres.Personnages.Heros;
using HerosVsMonstres.Personnages.Monstres;
using HerosVsMonstres.Recompenses;
using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres
{
    public class MoteurDeJeu
    {

        public MoteurDeJeu(Heros myHero)
        {
            jouerPartie(myHero);
        }

        public void jouerPartie(Heros monHero) 
        {
            Console.WriteLine("LA PARTIE VA COMMENCER ! \n ");

            Random nbAleatoire = new Random();
            Tresor tresor = new Tresor();
            Piege piege = new Piege();

            do
            {
                monHero.sePresente();

                Monstre monstreInvoque = GestionnaireDePartie.InvoquerMonstre(nbAleatoire.Next(10, 100), nbAleatoire.Next(10, 100));
                Console.WriteLine("Un(e) {0} apparait avec {1} points de vie et {2} points d'attaque !", monstreInvoque, monstreInvoque.Vie, monstreInvoque.Attaque);

                do
                {

                    monHero.attaque(monstreInvoque);
                    Console.WriteLine("Le/la {0} attaque le/la {1} ", monHero, monstreInvoque);
                    Console.WriteLine("Il reste {0} points de vie au {1}", monstreInvoque.Vie, monstreInvoque);
                    
                    if (monstreInvoque.EstVivant)
                    {
                        monstreInvoque.attaque(monHero);
                        Console.WriteLine("Le/la {0} attaque le/la {1} ", monstreInvoque, monHero);
                        Console.WriteLine("Il reste {0} points de vie à {1}", monHero.Vie, monHero);
                    }

                } while (monstreInvoque.EstVivant && monHero.EstVivant);

                if (monHero.EstVivant) { 
                    monHero.Score += 1; 
                    Console.WriteLine("Le {0} est mort", monstreInvoque);

                    switch (nbAleatoire.Next(3))
                    {
                        case 0:
                            tresor.recompenser(monHero);
                            break;
                        case 1:
                            piege.recompenser(monHero);
                            break;
                        case 2:
                            Console.WriteLine("Je n'ai bénéficié d'aucun bonus ce tour-ci !");
                            break;
                        default:
                            break;
                    }

                    if (monHero.EstVivant) if (monHero.Score == 5) monHero = GestionnaireDePartie.EvoluerHeros(monHero);

                }

                Console.WriteLine("\n");

            } while (monHero.EstVivant);

            Console.WriteLine(" \n" +
                "Malheureusement le {0} est mort après {1} victoire(s)", monHero, monHero.Score);
        }

    }
}
