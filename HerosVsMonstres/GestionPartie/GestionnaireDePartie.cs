﻿using HerosVsMonstres.Personnages.Heros;
using HerosVsMonstres.Personnages.Monstres;
using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres
{
    public class GestionnaireDePartie
    {

        public static Monstre InvoquerMonstre(int v_pVie, int v_pAttaque)
        {
            Random nbAleatoire = new Random();
            Monstre monstreInvoque;
            switch (nbAleatoire.Next(3))
            {
                case 0:
                    monstreInvoque = new Gobelin(v_pVie, v_pAttaque);
                    break;
                case 1:
                    monstreInvoque = new Sorciere(v_pVie, v_pAttaque);
                    break;
                case 2:
                    monstreInvoque = new Squelette(v_pVie, v_pAttaque);
                    break;
                default:
                    monstreInvoque = null;
                    break;
            }

            return monstreInvoque;
        }

        public static Heros EvoluerHeros(Heros v_heros)
        {
            Random nbAleatoire = new Random();

            switch (nbAleatoire.Next(3))
            {
                case 0:
                    v_heros = new Chevalier(v_heros);
                    break;
                case 1:
                    v_heros = new Archer(v_heros);
                    break;
                case 2:
                    v_heros = new Magicien(v_heros);
                    break;
                default:
                    v_heros = v_heros;
                    break;
            }

            return v_heros;
        }


    }
}
