﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages
{
    public abstract class Personnage
    {

        public int Vie { get; protected set; }
        public bool EstVivant { get; protected set; }
        public int Attaque { get; protected set; }
        
        public Personnage(int v_pVie, int v_pAttaque)
        {
            Vie = (v_pVie < 0) ? 0 : v_pVie;
            EstVivant = true;
            Attaque = (v_pAttaque < 0) ? 0 : v_pAttaque;
        }

        public void attaque(Personnage v_advervaire)
        {
            v_advervaire.seDefend(Attaque);
        } 

        public void seDefend(int v_attaqueAdversaire)
        {
            if (Vie > v_attaqueAdversaire) Vie -= v_attaqueAdversaire;
            else { Vie = 0; EstVivant = false; }
        }

        public override string ToString()
        {
            return "J'évolue en " + this.GetType().Name;
        }

        public void appliquerBonus(int v_bonusVie)
        {
            if (v_bonusVie > 0) Vie += v_bonusVie;
        }

        public void infligerMalus(int v_malusVie)
        {
            Vie = (v_malusVie > 0 && Vie - v_malusVie > 0) ? Vie - v_malusVie :  0;
            if (Vie == 0) EstVivant = false;
        }

    }
}
