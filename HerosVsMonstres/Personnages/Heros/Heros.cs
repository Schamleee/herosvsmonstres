﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages.Heros
{
    public class Heros : Personnage
    {
        public int Score { get; set; }

        public Heros(int v_pVie, int v_pAttaque) : base(v_pVie, v_pAttaque)
        {
            Score = 0;
        }

        public void sePresente()
        {
            Console.WriteLine("Je suis " + GetType().Name + " et j'ai " + Vie + " PV pour " + Attaque + " PA");
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}
