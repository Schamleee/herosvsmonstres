﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages.Heros
{
    public class Archer : Heros
    {

        public Archer(Heros v_heros) : base(v_heros.Vie, v_heros.Attaque)
        {
            Score = v_heros.Score;
            Attaque = Attaque += Convert.ToInt32(Attaque * 0.15);
            Vie = Vie += Convert.ToInt32(Vie * 0.15);
            Console.WriteLine("J'évoule en : " + ToString());
        }

    }
}
