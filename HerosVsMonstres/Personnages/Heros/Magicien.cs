﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages.Heros
{
    public class Magicien : Heros
    {
        public Magicien(Heros v_heros) : base(v_heros.Vie, v_heros.Attaque)
        {
            Score = v_heros.Score;
            Vie = Vie += Convert.ToInt32(Vie * 0.10);
            Attaque = Attaque += Convert.ToInt32(Attaque * 0.20);
            Console.WriteLine("J'évoule en : " + ToString());
        }
    }
}
