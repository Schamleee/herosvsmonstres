﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages.Heros
{
    public class Chevalier : Heros
    {

        public Chevalier(Heros v_heros) : base(v_heros.Vie, v_heros.Attaque)
        {
            Score = v_heros.Score;
            Vie = Vie += Convert.ToInt32(Vie * 0.20); 
            Attaque = Attaque += Convert.ToInt32(Attaque * 0.10);
            Console.WriteLine("J'évoule en : " + ToString());
        }
    }
}
