﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages.Monstres
{
    public class Sorciere : Monstre
    {
        public const double TAUX_VIE_SUP = -0.4;
        public const double TAUX_ATTAQUE_SUP = 0.30;

        public Sorciere(int v_pVie, int v_pAttaque) : base(Convert.ToInt32(v_pVie * (1 + TAUX_VIE_SUP)), Convert.ToInt32(v_pAttaque * (1 + TAUX_ATTAQUE_SUP))) { }

        public override string ToString() { return GetType().Name; }


    }
}
