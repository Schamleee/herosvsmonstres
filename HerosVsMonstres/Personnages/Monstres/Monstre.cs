﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerosVsMonstres.Personnages.Monstres
{
    public abstract class Monstre : Personnage
    {

        public Monstre(int v_pVie, int v_pAttaque) : base(v_pVie, v_pAttaque) { }

    }
}
